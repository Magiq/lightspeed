package lightspeed

import (
	"encoding/xml"

	"bitbucket.org/Magiq/lightspeed/lightspeed/model"
)

func (a *API) Account(params *GetParams) (*model.Account, error) {
	resp, err := a.get("Account", params)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var accountResp struct {
		Account model.Account `xml:"Account"`
	}

	if err := xml.NewDecoder(resp.Body).Decode(&accountResp); err != nil {
		return nil, err
	}

	return &accountResp.Account, nil
}
