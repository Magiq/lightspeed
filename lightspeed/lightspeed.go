package lightspeed

import (
	"fmt"
	"net/http"

	"github.com/google/go-querystring/query"
)

const (
	ApiURL = "https://api.lightspeedapp.com/API/"
)

func NewAPI(client *http.Client) *API {
	return &API{
		client: client,
	}
}

type API struct {
	client *http.Client
}

type GetParams struct {
	Limit       int64         `url:"limit"`
	Offset      int64         `url:"offset"`
	TimeStamp   query.Encoder `url:"timeStamp"`
	OrderBy     string        `url:"orderby"`
	OrderByDesc string        `url:"orderby_desc"`
}

func (a *API) get(path string, params *GetParams) (*http.Response, error) {
	url := fmt.Sprintf("%s%s.xml", ApiURL, path)

	if params != nil {
		q, err := query.Values(params)
		if err != nil {
			return nil, err
		}
		url = fmt.Sprintf("%s?%s", url, q.Encode())
	}

	return a.client.Get(url)
}
