package model

import "time"

type Sale struct {
	SaleID                int64     `xml:"saleID"`
	TimeStamp             time.Time `xml:"timeStamp"`
	DiscountPercent       int64     `xml:"discountPercent"`
	Completed             bool      `xml:"completed"`
	Archived              bool      `xml:"archived"`
	Voided                bool      `xml:"voided"`
	EnablePromotions      bool      `xml:"enablePromotions"`
	IsTaxInclusive        bool      `xml:"isTaxInclusive"`
	CreateTime            time.Time `xml:"createTime"`
	UpdateTime            time.Time `xml:"updateTime"`
	ReferenceNumber       string    `xml:"referenceNumber"`
	ReferenceNumberSource string    `xml:"referenceNumberSource"`
	Tax1Rate              float64   `xml:"tax1Rate"`
	Tax2Rate              float64   `xml:"tax2Rate"`
	Change                int64     `xml:"change"`
	ReceiptPreference     string    `xml:"receiptPreference"`
	DisplayableSubtotal   int64     `xml:"displayableSubtotal"`
	TicketNumber          int64     `xml:"ticketNumber"`
	CalcDiscount          float64   `xml:"calcDiscount"`
	CalcTotal             float64   `xml:"calcTotal"`
	CalcSubtotal          float64   `xml:"calcSubtotal"`
	CalcTaxable           float64   `xml:"calcTaxable"`
	CalcNonTaxable        float64   `xml:"calcNonTaxable"`
	CalcAvgCost           float64   `xml:"calcAvgCost"`
	CalcFIFOCost          float64   `xml:"calcFIFOCost"`
	CalcTax1              float64   `xml:"calcTax1"`
	CalcTax2              float64   `xml:"calcTax2"`
	CalcPayments          float64   `xml:"calcPayments"`
	Total                 float64   `xml:"total"`
	TotalDue              float64   `xml:"totalDue"`
	DisplayableTotal      float64   `xml:"displayableTotal"`
	Balance               float64   `xml:"balance"`
	CustomerID            int64     `xml:"customerID"`
	DiscountID            int64     `xml:"discountID"`
	EmployeeID            int64     `xml:"employeeID"`
	QuoteID               int64     `xml:"quoteID"`
	RegisterID            int64     `xml:"registerID"`
	ShipToID              int64     `xml:"shipToID"`
	ShopID                int64     `xml:"shopID"`
	TaxCategoryID         int64     `xml:"taxCategoryID"`
	TaxTotal              float64   `xml:"taxTotal"`
}
