package model

type Account struct {
	AccountID uint64 `xml:"accountID"`
	Name      string `xml:"name"`
}
