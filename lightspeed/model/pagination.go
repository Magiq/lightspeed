package model

type Pagination struct {
	Count  uint64
	Offset uint64
	Limit  uint64
}
