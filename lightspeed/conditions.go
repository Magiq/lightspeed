package lightspeed

import (
	"fmt"
	"net/url"
	"time"
)

type Gt struct {
	Value interface{}
}

func (g Gt) EncodeValues(key string, v *url.Values) error {
	switch t := g.Value.(type) {
	case time.Time:
		v.Set(key, fmt.Sprintf(">,%s", t.Format(time.RFC3339)))
	case string:
		v.Set(key, fmt.Sprintf(">,%s", t))
	}

	return nil
}
