package lightspeed

import (
	"encoding/xml"
	"fmt"

	"bitbucket.org/Magiq/lightspeed/lightspeed/model"
)

func (a *API) Sales(accountID uint64, params *GetParams) ([]model.Sale, error) {
	resp, err := a.get(fmt.Sprintf("Account/%d/Sale", accountID), params)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var saleResp struct {
		Sale []model.Sale `xml:"Sale"`
	}

	if err := xml.NewDecoder(resp.Body).Decode(&saleResp); err != nil {
		return nil, err
	}

	return saleResp.Sale, nil
}
