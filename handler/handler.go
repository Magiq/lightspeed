package handler

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/Magiq/lightspeed/lightspeed"
	"bitbucket.org/Magiq/lightspeed/lightspeed/model"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

func init() {
	gob.Register(&oauth2.Token{})
	gob.Register(time.Time{})
}

type handler struct {
	oauthConfig *oauth2.Config
	cookie      *sessions.CookieStore
	*http.ServeMux
}

func New(oauthConfig *oauth2.Config) *handler {
	mux := http.NewServeMux()
	h := &handler{
		oauthConfig: oauthConfig,
		cookie:      sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY"))),
		ServeMux:    mux,
	}

	mux.HandleFunc("/", h.Sales)
	mux.HandleFunc("/login", h.Login)
	mux.HandleFunc("/oauth/redirect", h.Redirect)

	return h
}

func (h *handler) Login(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, fmt.Sprintf(`<a href="%s">Login</a>`, h.oauthConfig.AuthCodeURL("state", oauth2.AccessTypeOffline)))
}

func (h *handler) Redirect(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	session, err := h.cookie.Get(r, "app")
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	token, err := h.oauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	session.Values["token"] = token
	if err := session.Save(r, w); err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", 301)
}

func (h *handler) Sales(w http.ResponseWriter, r *http.Request) {
	session, err := h.cookie.Get(r, "app")
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	token, ok := session.Values["token"].(*oauth2.Token)
	if !ok {
		http.Redirect(w, r, "/login", 301)
		return
	}

	lighspeedAPI := lightspeed.NewAPI(h.oauthConfig.Client(oauth2.NoContext, token))

	account, err := lighspeedAPI.Account(nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var sales []model.Sale
	if timeStamp, ok := session.Values["timeStamp"].(time.Time); ok {
		sales, err = lighspeedAPI.Sales(account.AccountID, &lightspeed.GetParams{
			Limit:     100,
			TimeStamp: lightspeed.Gt{Value: timeStamp},
		})
	} else {
		sales, err = lighspeedAPI.Sales(account.AccountID, nil)
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(sales) > 0 {
		session.Values["timeStamp"] = sales[len(sales)-1].TimeStamp
		if err := session.Save(r, w); err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	if err := json.NewEncoder(w).Encode(sales); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
