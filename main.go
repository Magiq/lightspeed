package main

import (
	"net/http"
	"os"

	"bitbucket.org/Magiq/lightspeed/handler"

	"golang.org/x/oauth2"
)

var (
	oauthConfig *oauth2.Config
)

func init() {
	oauthConfig = &oauth2.Config{
		ClientID:     os.Getenv("CLIENT_ID"),
		ClientSecret: os.Getenv("CLIENT_SECRET"),
		Scopes:       []string{"employee:all"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://cloud.lightspeedapp.com/oauth/authorize.php",
			TokenURL: "https://cloud.lightspeedapp.com/oauth/access_token.php",
		},
	}
}

func main() {
	http.ListenAndServeTLS(":8888", "server.crt", "server.key", handler.New(oauthConfig))
}
