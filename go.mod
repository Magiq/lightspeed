module bitbucket.org/Magiq/lightspeed

require (
	github.com/google/go-querystring v1.0.0
	github.com/gorilla/sessions v1.1.3
	golang.org/x/oauth2 v0.0.0-20190130055435-99b60b757ec1
)
